﻿using System;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using SharpDX;
using Quaternion = SharpDX.Quaternion;
using Point = System.Windows.Point;
using Media3D = System.Windows.Media.Media3D;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Common;

namespace Jelly
{
    public class SimulationManager
    {
        public Helix Helix { get; set; } = new Helix();

        public SimulationConfiguration Configuration => Helix.Configuration;

        private DispatcherTimer dispatcherTimer;

        const int SIMULATION_SIZE = SimulationConfiguration.POINTS;
        const float OFFSET = (SIMULATION_SIZE - 1) / 2.0f;
        SimulationPoint[][][] points;
        Spring[] springs;
        Spring[] springs2;
        const float precision = 0.05f;

        internal void StartSimualtion()
        {
            dispatcherTimer?.Stop();
            Helix.Reset();
            InitializeSimulation();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)(precision * 100));
            dispatcherTimer.Start();
        }

        private void InitializeSimulation()
        {
            points = new SimulationPoint[SIMULATION_SIZE][][];
            for (int i = 0; i < SIMULATION_SIZE; i++)
            {
                points[i] = new SimulationPoint[SIMULATION_SIZE][];
                for (int j = 0; j < SIMULATION_SIZE; j++)
                {
                    points[i][j] = new SimulationPoint[SIMULATION_SIZE];
                    for (int k = 0; k < SIMULATION_SIZE; k++)
                    {
                        points[i][j][k] = new SimulationPoint(i - OFFSET, j - OFFSET, k - OFFSET);
                    }
                }
            }

            var spr = new List<Spring>();
            var IT = SIMULATION_SIZE - 1;
            var c1 = Configuration.ElasticyCoefficient1;
            var sqrt2 = (float)Math.Sqrt(2);
            for (int i = 0; i < SIMULATION_SIZE; i++)
            {
                for (int j = 0; j < SIMULATION_SIZE; j++)
                {
                    for (int k = 0; k < IT; k++)
                    {
                        spr.Add(new Spring(i, j, k, i, j, k + 1, c1, 1));
                        spr.Add(new Spring(j, k, i, j, k + 1, i, c1, 1));
                        spr.Add(new Spring(k, i, j, k + 1, i, j, c1, 1));
                    }
                }
            }
            for (int i = 0; i < SIMULATION_SIZE; i++)
            {
                for (int j = 0; j < IT; j++)
                {
                    for (int k = 0; k < IT; k++)
                    {
                        spr.Add(new Spring(i, j, k, i, j + 1, k + 1, c1, sqrt2));
                        spr.Add(new Spring(i, j + 1, k, i, j, k + 1, c1, sqrt2));
                        spr.Add(new Spring(j, k, i, j + 1, k + 1, i, c1, sqrt2));
                        spr.Add(new Spring(j + 1, k, i, j, k + 1, i, c1, sqrt2));
                        spr.Add(new Spring(k, i, j, k + 1, i, j + 1, c1, sqrt2));
                        spr.Add(new Spring(k, i, j + 1, k + 1, i, j, c1, sqrt2));

                    }
                }
            }
            springs = spr.ToArray();
            spr.Clear();
            var c2 = Configuration.ElasticyCoefficient2;
            spr.Add(new Spring(0, 0, 0, c2, 0));
            spr.Add(new Spring(3, 0, 0, c2, 0));
            spr.Add(new Spring(0, 3, 0, c2, 0));
            spr.Add(new Spring(0, 0, 3, c2, 0));
            spr.Add(new Spring(0, 3, 3, c2, 0));
            spr.Add(new Spring(3, 0, 3, c2, 0));
            spr.Add(new Spring(3, 3, 0, c2, 0));
            spr.Add(new Spring(3, 3, 3, c2, 0));

            springs2 = spr.ToArray();
        }

        internal void MoveFrame(Vector delta)
        {
            var right = Vector3D.CrossProduct(Camera.LookDirection, Helix.UpDirection);
            right.Normalize();
            var up = Vector3D.CrossProduct(right, Camera.LookDirection);
            up.Normalize();

            FramePos += 0.02f * (delta.X * right - delta.Y * up);

        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            var k1 = Configuration.DampingRatio;

            foreach (var s in springs)
            {
                var forceDir = points[s.P1I1][s.P1I2][s.P1I3].Position - points[s.P2I1][s.P2I2][s.P2I3].Position;
                var f = s.Force(forceDir.Length());
                forceDir.Normalize();

                points[s.P1I1][s.P1I2][s.P1I3].Force += forceDir * f;
                points[s.P2I1][s.P2I2][s.P2I3].Force -= forceDir * f;
            }
            var framePos = FramePos.ToVector3();
            foreach (var s in springs2)
            {
                var forceDir = points[s.P1I1][s.P1I2][s.P1I3].Position - (framePos + new Vector3(Offset(s.P1I1), Offset(s.P1I2), Offset(s.P1I3)));
                var f = s.Force(forceDir.Length());
                forceDir.Normalize();

                points[s.P1I1][s.P1I2][s.P1I3].Force += forceDir * f;
            }
            for (int i = 0; i < SIMULATION_SIZE; i++)
            {
                for (int j = 0; j < SIMULATION_SIZE; j++)
                {
                    for (int k = 0; k < SIMULATION_SIZE; k++)
                    {
                        points[i][j][k].Next(precision, SimulationConfiguration.MAX, k1);
                    }
                }
            }

            Helix.SetState(points, FramePos);
        }

        float Offset(int idx) => idx == 0 ? -OFFSET : OFFSET;

        public HelixToolkit.Wpf.SharpDX.PerspectiveCamera Camera => Helix.Camera;

        public Vector3D FramePos { get; private set; } = new Vector3D(0, 0, 0);



    }
}
