﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Jelly
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        SimulationManager Manager => DataContext as SimulationManager;

        private void StartSimulationButton_Click(object sender, RoutedEventArgs e)
        {
            Manager.StartSimualtion();
        }

        Point lastPos = new Point(0,0);
        bool lastOk = false;

        private void Border_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
            {
                lastOk = false;
            }
            else
            {
                var pos = e.GetPosition(this);
                if(lastOk == false)
                {
                    lastPos = pos;
                    lastOk = true;
                    return;
                }
                Manager.MoveFrame(pos - lastPos);
                lastPos = pos;
            }

        }

        private void LoadObjButton_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "obj files (*.obj)|*.obj|All files (*.*)|*.*";
            if(ofd.ShowDialog() == true)
            {
                Manager.Helix.ReadObj(ofd.FileName);
            }
        }
    }
}
