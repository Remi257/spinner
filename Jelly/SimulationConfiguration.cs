﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jelly
{
    public class SimulationConfiguration : ObservableObject
    {
        private bool isControlGridVisible = false;
        public bool IsControlGridVisible { get => isControlGridVisible; set => SetValue(ref isControlGridVisible, value); }

        private bool isFrameVisible = true;
        public bool IsFrameVisible { get => isFrameVisible; set => SetValue(ref isFrameVisible, value); }

        private bool isLimitingCuboidVisible = true;
        public bool IsLimitingCuboidVisible { get => isLimitingCuboidVisible; set => SetValue(ref isLimitingCuboidVisible, value); }

        private bool isBezierCubeVisible = false;
        public bool IsBezierCubeVisible { get => isBezierCubeVisible; set => SetValue(ref isBezierCubeVisible, value); }

        private bool isBezierShapeVisible = false;
        public bool IsBezierShapeVisible { get => isBezierShapeVisible; set => SetValue(ref isBezierShapeVisible, value); }

        private float pointMass =1;
        public float PointMass { get => pointMass; set => SetValue(ref pointMass, value); }

        private float elasticyCoefficient1 = 1;
        public float ElasticyCoefficient1 { get => elasticyCoefficient1; set => SetValue(ref elasticyCoefficient1, value); }

        private float elasticyCoefficient2 = 2;
        public float ElasticyCoefficient2 { get => elasticyCoefficient2; set => SetValue(ref elasticyCoefficient2, value); }

        private float dampingRatio = 1;
        public float DampingRatio { get => dampingRatio; set => SetValue(ref dampingRatio, value); }

        private float measureOfDisorder = 1;
        public float MeasureOfDisorder { get => measureOfDisorder; set => SetValue(ref measureOfDisorder, value); }

        public const int MAX = 4;
        public const int POINTS = 4;
    }
}
