﻿using System.Linq;
using HelixToolkit.Wpf.SharpDX;
using Media3D = System.Windows.Media.Media3D;
using Point3D = System.Windows.Media.Media3D.Point3D;
using Vector3D = System.Windows.Media.Media3D.Vector3D;
using Transform3D = System.Windows.Media.Media3D.Transform3D;
using Color = System.Windows.Media.Color;
using Vector3 = SharpDX.Vector3;
using Colors = System.Windows.Media.Colors;
using System.IO;
using System.Windows.Input;
using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using SharpDX;

namespace Jelly
{
    public class Helix : ObservableObject
    {
        public readonly Media3D.Quaternion DefaultQuaternion = new Media3D.Quaternion(new Vector3D(1, -1, 0), Degrees(Math.Acos(Math.Sqrt(3) / 3)));
        public readonly Media3D.RotateTransform3D DefaultTransform = new Media3D.RotateTransform3D(new Media3D.QuaternionRotation3D(new Media3D.Quaternion(new Vector3D(1, -1, 0), Degrees(Math.Acos(Math.Sqrt(3) / 3)))));

        readonly Queue<Vector3D> queue = new Queue<Vector3D>();

        internal void Reset()
        {

        }

        #region INotify_fields
        private LineGeometry3D springs;
        private Transform3D frameBoxTransform;
        private MeshGeometry3D bezierCubeFace1;
        private MeshGeometry3D bezierCubeFace2;
        private MeshGeometry3D bezierCubeFace3;
        private MeshGeometry3D bezierCubeFace4;
        private MeshGeometry3D bezierCubeFace5;
        private MeshGeometry3D bezierCubeFace6;
        private Geometry3D bezierShapeGeometry;
        private Geometry3D[] objGeometry;

        #endregion

        #region Properties
        public LineGeometry3D BoundingBox { get; private set; }
        public Transform3D BoundingBoxTransform { get; private set; }
        public LineGeometry3D FrameBox { get; private set; }
        public Transform3D FrameBoxTransform { get => frameBoxTransform; private set => SetValue(ref frameBoxTransform, value); }
        public PointGeometry3D BezierPoints { get; private set; }
        public LineGeometry3D Springs { get => springs; private set => SetValue(ref springs, value); }
        public Transform3D SpringsTransform { get; private set; }
        public MeshGeometry3D BezierCubeFace1 { get => bezierCubeFace1; private set => SetValue(ref bezierCubeFace1, value); }
        public MeshGeometry3D BezierCubeFace2 { get => bezierCubeFace2; private set => SetValue(ref bezierCubeFace2, value); }
        public MeshGeometry3D BezierCubeFace3 { get => bezierCubeFace3; private set => SetValue(ref bezierCubeFace3, value); }
        public MeshGeometry3D BezierCubeFace4 { get => bezierCubeFace4; private set => SetValue(ref bezierCubeFace4, value); }
        public MeshGeometry3D BezierCubeFace5 { get => bezierCubeFace5; private set => SetValue(ref bezierCubeFace5, value); }
        public MeshGeometry3D BezierCubeFace6 { get => bezierCubeFace6; private set => SetValue(ref bezierCubeFace6, value); }
        public Transform3D BezierCubeTransform { get; private set; }
        public PhongMaterial RedMaterial { get; private set; } = PhongMaterials.Red;
        public Geometry3D BezierShapeGeometry { get => bezierShapeGeometry; private set => SetValue(ref bezierShapeGeometry, value); }
        public ObservableElement3DCollection YourElement3DCollection { get; set; } = new ObservableElement3DCollection();
        public Vector3D DirectionalLightDirection { get; private set; }
        public LineGeometry3D Grid { get; private set; }
        public Transform3D GridTransform { get; private set; }
        public Color DirectionalLightColor { get; private set; }
        public Color AmbientLightColor { get; private set; }
        public Vector3D UpDirection { set; get; } = new Vector3D(0, 0, 1);
        public string Title { get; }
        public string SubTitle { get; }
        public PerspectiveCamera Camera { get; private set; }
        public DefaultEffectsManager EffectsManager { get; private set; }
        public SimulationConfiguration Configuration { get; set; } = new SimulationConfiguration();
        #endregion

        public Helix()
        {
            EffectsManager = new DefaultEffectsManager();
            // titles
            Title = "Jelly";
            SubTitle = "WPF & SharpDX";

            // camera setup
            Camera = new PerspectiveCamera
            {
                Position = new Point3D(3, 3, 5),
                LookDirection = new Vector3D(-3, -3, -5),
                UpDirection = new Vector3D(0, 0, 1),
                FarPlaneDistance = 500000
            };



            // setup lighting            
            AmbientLightColor = Colors.DimGray;
            DirectionalLightColor = Colors.White;
            DirectionalLightDirection = new Vector3D(-2, -5, -2);

            // bounding box
            float bbsize = SimulationConfiguration.MAX;
            var mb = new MeshBuilder();
            mb.AddBoundingBox(new Media3D.Rect3D(-bbsize, -bbsize, -bbsize, 2 * bbsize, 2 * bbsize, 2 * bbsize), 0.2);
            BoundingBox = LineBuilder.GenerateBoundingBox(mb.ToMeshGeometry3D());
            BoundingBoxTransform = Transform3D.Identity;

            // floor plane grid
            Grid = LineBuilder.GenerateGrid(new Vector3(0, 0, 1), -5, 5);
            GridTransform = new Media3D.TranslateTransform3D(0, 0, -1.3 * bbsize);

            // mass frame
            bbsize = (SimulationConfiguration.POINTS - 1) / 2.0f;
            mb = new MeshBuilder();
            mb.AddBoundingBox(new Media3D.Rect3D(-bbsize, -bbsize, -bbsize, 2 * bbsize, 2 * bbsize, 2 * bbsize), 0.2);
            FrameBox = LineBuilder.GenerateBoundingBox(mb.ToMeshGeometry3D());
            FrameBoxTransform = Transform3D.Identity;

            BezierPoints = new PointGeometry3D();
            SpringsTransform = Transform3D.Identity;


            BezierCubeTransform = Transform3D.Identity;
        }

        internal void ReadObj(string path)
        {
            YourElement3DCollection.Clear();
            var reader = new ObjReader();
            var objList = reader.Read(path);
            var max = objList.Max(o => o.Geometry.Positions.Max(v => Math.Max(Math.Max(Math.Abs(v.X), Math.Abs(v.Y)), Math.Abs(v.Z))));

            objList.ForEach(o =>
            {
                for (int i = 0; i < o.Geometry.Positions.Count; i++)
                {
                    o.Geometry.Positions[i] = (o.Geometry.Positions[i] / max + 1) / 2;
                }
            });
#warning here is shader type changed
            if (false)
            {
                foreach (var ob in objList)
                {
                    YourElement3DCollection.Add(new CustomBezierMeshModel3D()
                    {
                        Geometry = ob.Geometry,
                        Material = RedMaterial
                    });
                }

            }
            else
            {
                objGeometry = objList.Select(o => o.Geometry).ToArray();
                foreach (var ob in objList)
                {
                    var meshGeometry = new MeshGeometryModel3D
                    {
                        Geometry = ob.Geometry,
                        Material = RedMaterial
                    };
                    YourElement3DCollection.Add(meshGeometry);

                    // Run this line if you are using a render host
                }
            }
            OnPropertyChanged(nameof(YourElement3DCollection));

        }


        private static double Degrees(double radians) => 180 * radians / Math.PI;

        internal void SetState(SimulationPoint[][][] positions, Vector3D framePos)
        {
            if (Configuration.IsControlGridVisible)
            {

                int size = SimulationConfiguration.POINTS;
                int it = size - 1;
                BezierPoints.Positions = new Vector3Collection(positions.SelectMany(v => v).SelectMany(v => v).Select(p => p.Position));
                var lb = new LineBuilder();
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        for (int k = 0; k < it; k++)
                        {
                            lb.AddLine(positions[i][j][k].Position, positions[i][j][k + 1].Position);
                            lb.AddLine(positions[j][k][i].Position, positions[j][k + 1][i].Position);
                            lb.AddLine(positions[k][i][j].Position, positions[k + 1][i][j].Position);
                        }
                    }
                }
                //for (int i = 0; i < size; i++)
                //{
                //    for (int j = 0; j < it; j++)
                //    {
                //        for (int k = 0; k < it; k++)
                //        {
                //            lb.AddLine(positions[i][j][k].Position, positions[i][j + 1][k + 1].Position);
                //            lb.AddLine(positions[i][j + 1][k].Position, positions[i][j][k + 1].Position);
                //            lb.AddLine(positions[j][k][i].Position, positions[j + 1][k + 1][i].Position);
                //            lb.AddLine(positions[j + 1][k][i].Position, positions[j][k + 1][i].Position);
                //            lb.AddLine(positions[k][i][j].Position, positions[k + 1][i][j + 1].Position);
                //            lb.AddLine(positions[k][i][j + 1].Position, positions[k + 1][i][j].Position);

                //        }
                //    }
                //}

                Springs = lb.ToLineGeometry3D();
            }
            if (Configuration.IsBezierCubeVisible)
            {
                var pts = new Vector3[4][];
                for (int i = 0; i < 4; i++)
                {
                    pts[i] = new Vector3[4];
                    for (int j = 0; j < 4; j++)
                    {
                        pts[i][j] = positions[j][i][0].Position;
                    }
                }
                BezierCubeFace1 = CalculateBezierFace((u, v) =>
                {
                    var res = Vector3.Zero;
                    for (int i = 0; i < basis.Length; i++)
                    {
                        for (int j = 0; j < basis.Length; j++)
                        {
                            res += basis[i](u) * basis[j](v) * pts[i][j];
                        }
                    }
                    return res;
                });
                pts = new Vector3[4][];
                for (int i = 0; i < 4; i++)
                {
                    pts[i] = new Vector3[4];
                    for (int j = 0; j < 4; j++)
                    {
                        pts[i][j] = positions[i][j][3].Position;
                    }
                }
                BezierCubeFace2 = CalculateBezierFace((u, v) =>
                {
                    var res = Vector3.Zero;
                    for (int i = 0; i < basis.Length; i++)
                    {
                        for (int j = 0; j < basis.Length; j++)
                        {
                            res += basis[i](u) * basis[j](v) * pts[i][j];
                        }
                    }
                    return res;
                });
                pts = new Vector3[4][];
                for (int i = 0; i < 4; i++)
                {
                    pts[i] = new Vector3[4];
                    for (int j = 0; j < 4; j++)
                    {
                        pts[i][j] = positions[0][j][i].Position;
                    }
                }
                BezierCubeFace3 = CalculateBezierFace((u, v) =>
                {
                    var res = Vector3.Zero;
                    for (int i = 0; i < basis.Length; i++)
                    {
                        for (int j = 0; j < basis.Length; j++)
                        {
                            res += basis[i](u) * basis[j](v) * pts[i][j];
                        }
                    }
                    return res;
                });
                pts = new Vector3[4][];
                for (int i = 0; i < 4; i++)
                {
                    pts[i] = new Vector3[4];
                    for (int j = 0; j < 4; j++)
                    {
                        pts[i][j] = positions[3][i][j].Position;
                    }
                }
                BezierCubeFace4 = CalculateBezierFace((u, v) =>
                {
                    var res = Vector3.Zero;
                    for (int i = 0; i < basis.Length; i++)
                    {
                        for (int j = 0; j < basis.Length; j++)
                        {
                            res += basis[i](u) * basis[j](v) * pts[i][j];
                        }
                    }
                    return res;
                });
                pts = new Vector3[4][];
                for (int i = 0; i < 4; i++)
                {
                    pts[i] = new Vector3[4];
                    for (int j = 0; j < 4; j++)
                    {
                        pts[i][j] = positions[i][0][j].Position;
                    }
                }
                BezierCubeFace5 = CalculateBezierFace((u, v) =>
                {
                    var res = Vector3.Zero;
                    for (int i = 0; i < basis.Length; i++)
                    {
                        for (int j = 0; j < basis.Length; j++)
                        {
                            res += basis[i](u) * basis[j](v) * pts[i][j];
                        }
                    }
                    return res;
                });
                pts = new Vector3[4][];
                for (int i = 0; i < 4; i++)
                {
                    pts[i] = new Vector3[4];
                    for (int j = 0; j < 4; j++)
                    {
                        pts[i][j] = positions[j][3][i].Position;
                    }
                }
                BezierCubeFace6 = CalculateBezierFace((u, v) =>
                {
                    var res = Vector3.Zero;
                    for (int i = 0; i < basis.Length; i++)
                    {
                        for (int j = 0; j < basis.Length; j++)
                        {
                            res += basis[i](u) * basis[j](v) * pts[i][j];
                        }
                    }
                    return res;
                });

            }
            if (Configuration.IsFrameVisible) FrameBoxTransform = new Media3D.TranslateTransform3D(framePos);
            if (Configuration.IsBezierShapeVisible && objGeometry != null)
            {
                bezierPts = new Vector3[4][][];
                for (int i = 0; i < 4; i++)
                {
                    bezierPts[i] = new Vector3[4][];
                    for (int j = 0; j < 4; j++)
                    {
                        bezierPts[i][j] = new Vector3[4];
                        for (int k = 0; k < 4; k++)
                        {
                            bezierPts[i][j][k] = positions[i][j][k].Position;

                        }
                    }
                }
                YourElement3DCollection.Clear();
                foreach (var g in objGeometry)
                {
                    MeshGeometryModel3D m = new MeshGeometryModel3D();
                    MeshGeometry3D mesh = new MeshGeometry3D();
                    Vector3[] tmp = new Vector3[g.Positions.Count];
                    g.Positions.CopyTo(tmp);
                    mesh.Positions = new Vector3Collection(tmp);
                    int[] tmp2 = new int[g.Indices.Count];
                    g.Indices.CopyTo(tmp2);
                    mesh.Indices = new IntCollection(tmp2);
                    for (int i = 0; i < mesh.Positions.Count; i++)
                    {
                        mesh.Positions[i] = Transform(mesh.Positions[i]);
                    }
                    mesh.Normals = CalculateNormals(mesh.Positions, mesh.Indices);
                    m.Geometry = mesh;
                    m.Material = RedMaterial;
                    YourElement3DCollection.Add(m);
                }
                OnPropertyChanged(nameof(YourElement3DCollection));
            }
        }

        Vector3[][][] bezierPts;

        private Vector3 Transform(Vector3 v)
        {
            var res = Vector3.Zero;
            if (v.AllSmallerOrEqual(1.0f) && v.AllGreaterOrEqual(-1.0f))
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {
                        for (int k = 0; k < 4; k++)
                        {
                            res += basis[i](v.X) * basis[j](v.Y) * basis[k](v.Z) * bezierPts[i][j][k];
                        }
                    }
                }
                return res;
            }
            else throw new ArgumentException("niedobrze");
        }

        readonly int MeshSizeU = 10;
        readonly int MeshSizeV = 10;
        Func<float, float>[] basis = new Func<float, float>[]
            {
                t => (1 - t) * (1 - t) * (1 - t),
                t => 3 * (1 - t) * (1 - t) * t,
                t => 3 * (1 - t) * t * t,
                t => t * t * t
            };

        protected virtual MeshGeometry3D CalculateBezierFace(Func<float, float, Vector3> Evaluate)
        {
            var mesh = new MeshGeometry3D()
            {
                Positions = new Vector3Collection(),
                Normals = new Vector3Collection(),
                Indices = new IntCollection()
            };
            int n = this.MeshSizeU;
            int m = this.MeshSizeV;
            var p = new Vector3[m * n];

            for (int i = 0; i < n; i++)
            {
                float u = 1.0f * i / (n - 1);

                for (int j = 0; j < m; j++)
                {
                    float v = 1.0f * j / (m - 1);
                    int ij = (i * m) + j;
                    p[ij] = Evaluate(u, v);
                }
            }
            int idx = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++, idx++)
                {
                    mesh.Positions.Add(p[idx]);
                }
            }
            for (int i = 0; i + 1 < n; i++)
            {
                for (int j = 0; j + 1 < m; j++)
                {
                    int x0 = i * m;
                    int x1 = (i + 1) * m;
                    int y0 = j;
                    int y1 = j + 1;
                    AddTriangle(mesh, x0 + y0, x1 + y0, x0 + y1);
                    AddTriangle(mesh, x1 + y0, x1 + y1, x0 + y1);
                }
            }
            mesh.Normals = CalculateNormals(mesh.Positions, mesh.Indices);

            return mesh;
        }

        private static void AddTriangle(MeshGeometry3D mesh, int i1, int i2, int i3)
        {
            var p1 = mesh.Positions[i1];
            if (!IsDefined(p1))
            {
                return;
            }

            var p2 = mesh.Positions[i2];
            if (!IsDefined(p2))
            {
                return;
            }

            var p3 = mesh.Positions[i3];
            if (!IsDefined(p3))
            {
                return;
            }

            mesh.TriangleIndices.Add(i1);
            mesh.TriangleIndices.Add(i2);
            mesh.TriangleIndices.Add(i3);
        }

        private static bool IsDefined(Vector3 point)
        {
            return !double.IsNaN(point.X) && !double.IsNaN(point.Y) && !double.IsNaN(point.Z);
        }

        public static Vector3Collection CalculateNormals(Vector3Collection positions, IntCollection triangleIndices)
        {
            var normals = new Vector3Collection(positions.Count);
            for (int i = 0; i < positions.Count; i++)
            {
                normals.Add(new Vector3());
            }

            for (int i = 0; i < triangleIndices.Count; i += 3)
            {
                int index0 = triangleIndices[i];
                int index1 = triangleIndices[i + 1];
                int index2 = triangleIndices[i + 2];
                var p0 = positions[index0];
                var p1 = positions[index1];
                var p2 = positions[index2];
                Vector3 u = p1 - p0;
                Vector3 v = p2 - p0;
                Vector3 w = Vector3.Cross(u, v);
                w.Normalize();
                normals[index0] += w;
                normals[index1] += w;
                normals[index2] += w;
            }

            normals.ForEach(n => n.Normalize());

            return normals;
        }
    }
}
