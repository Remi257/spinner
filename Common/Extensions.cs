﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using SharpDX;
using Media3D = System.Windows.Media.Media3D;

namespace Common
{
    public static class Extensions
    {
        public static Vector3 ToVector3(this Vector3D v)
        {
            return new Vector3((float)v.X, (float)v.Y, (float)v.Z);
        }
        public static Vector3D ToVector3D(this Vector3 v)
        {
            return new Vector3D(v.X, v.Y, v.Z);
        }

        public static SharpDX.Quaternion ToSDXQuaternion(this Media3D.Quaternion q)
        {
            return new SharpDX.Quaternion((float)q.X, (float)q.Y, (float)q.Z, (float)q.W);
        }
        public static Media3D.Quaternion ToMedia3DQuaternion(this SharpDX.Quaternion q)
        {
            return new Media3D.Quaternion(q.X, q.Y, q.Z, q.W);
        }

    }
}
