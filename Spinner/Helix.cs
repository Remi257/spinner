﻿using System.Linq;
using HelixToolkit.Wpf.SharpDX;
using Media3D = System.Windows.Media.Media3D;
using Point3D = System.Windows.Media.Media3D.Point3D;
using Vector3D = System.Windows.Media.Media3D.Vector3D;
using Transform3D = System.Windows.Media.Media3D.Transform3D;
using Color = System.Windows.Media.Color;
using Vector3 = SharpDX.Vector3;
using Colors = System.Windows.Media.Colors;
using System.Windows.Input;
using System;
using System.Collections.Generic;
//using Common;

namespace Spinner
{
    public class Helix : Common.ObservableObject
    {
        public readonly Media3D.Quaternion DefaultQuaternion = new Media3D.Quaternion(new Vector3D(1, -1, 0), Degrees(Math.Acos(Math.Sqrt(3) / 3)));
        public readonly Media3D.RotateTransform3D DefaultTransform = new Media3D.RotateTransform3D(new Media3D.QuaternionRotation3D(new Media3D.Quaternion(new Vector3D(1, -1, 0), Degrees(Math.Acos(Math.Sqrt(3) / 3)))));

        readonly Queue<Vector3D> queue = new Queue<Vector3D>();

        internal void Reset()
        {
            queue.Clear();

            // scene model3d
            var b1 = new MeshBuilder();
            var a = Configuration.CubeSize / 2;
            b1.AddBox(new Vector3(a, a, a), 2*a, 2*a, 2*a, BoxFaces.All);

            var meshGeometry = b1.ToMeshGeometry3D();
            meshGeometry.Colors = new Color4Collection(meshGeometry.TextureCoordinates.Select(x => x.ToColor4()));
            Cube = meshGeometry;

            CubeTransform = DefaultTransform;

            var e1 = new LineBuilder();
            e1.Add(false, new Vector3(0, 0, 0), new Vector3(2*a, 2*a, 2*a));
            Diagonal = e1.ToLineGeometry3D();
        }

        #region Properties
        private MeshGeometry3D cube;
        public MeshGeometry3D Cube { get => cube; private set => SetValue(ref cube, value); }
        public LineGeometry3D Gravity { get; private set; }
        private LineGeometry3D diagonal;
        public LineGeometry3D Diagonal { get => diagonal; private set => SetValue(ref diagonal, value); }
        private PointGeometry3D trajectory;
        public PointGeometry3D Trajectory { get => trajectory; private set => SetValue(ref trajectory, value); }
        public PhongMaterial RedMaterial { get; private set; }
        private Transform3D cubeTransform;
        public Transform3D CubeTransform { get => cubeTransform; private set => SetValue(ref cubeTransform, value); }
        public Transform3D GridTransform { get; private set; }
        public Transform3D GravityTransform { get; private set; }
        public Vector3D DirectionalLightDirection { get; private set; }
        public LineGeometry3D Grid { get; private set; }
        public Color DirectionalLightColor { get; private set; }
        public Color AmbientLightColor { get; private set; }
        public Vector3D UpDirection { set; get; } = new Vector3D(0, 0, 1);
        public ICommand UpXCommand { private set; get; }
        public ICommand UpYCommand { private set; get; }
        public ICommand UpZCommand { private set; get; }
        public string Title { get; }
        public string SubTitle { get; }
        public PerspectiveCamera Camera { get; private set; }
        public DefaultEffectsManager EffectsManager { get; private set; }
        public SimulationConfiguration Configuration { get; set; } = new SimulationConfiguration();
        #endregion

        public Helix()
        {


            EffectsManager = new DefaultEffectsManager();
            // titles
            Title = "Spinner";
            SubTitle = "WPF & SharpDX";

            // camera setup
            Camera = new PerspectiveCamera
            {
                Position = new Point3D(3, 3, 5),
                LookDirection = new Vector3D(-3, -3, -5),
                UpDirection = new Vector3D(0, 0, 1),
                FarPlaneDistance = 5000000
            };

            // setup lighting            
            AmbientLightColor = Colors.DimGray;
            DirectionalLightColor = Colors.White;
            DirectionalLightDirection = new Vector3D(-2, -5, -2);

            // floor plane grid
            Grid = LineBuilder.GenerateGrid(new Vector3(0, 0, 1), -5, 5);
            GridTransform = new Media3D.TranslateTransform3D(0, 0, 0);

            // scene model3d
            var b1 = new MeshBuilder();
            b1.AddCube(BoxFaces.All);

            var meshGeometry = b1.ToMeshGeometry3D();
            meshGeometry.Colors = new Color4Collection(meshGeometry.TextureCoordinates.Select(x => x.ToColor4()));
            Cube = meshGeometry;

            CubeTransform = DefaultTransform;
            //CubeTransform = Transform3D.Identity;
            RedMaterial = PhongMaterials.White;
            var diffuse = RedMaterial.DiffuseColor;
            diffuse.Alpha = 0.8f;
            RedMaterial.DiffuseColor = diffuse;

            var e1 = new LineBuilder();
            e1.Add(false, new Vector3(0, 0, 0), new Vector3(1, 1, 1));
            Diagonal = e1.ToLineGeometry3D();

            Trajectory = new PointGeometry3D();

            Gravity = new LineGeometry3D();
            GravityTransform = Transform3D.Identity;
        }

        private static double Degrees(double radians) => 180 * radians / Math.PI;

        internal void SetState(Media3D.Quaternion q)
        {
            CubeTransform = new Media3D.RotateTransform3D(new Media3D.QuaternionRotation3D(q));

            var size = Configuration.CubeSize;
            var pt = CubeTransform.Transform(new Vector3D(size, size, size));
            if (queue.Count >= Configuration.TrajectoryLength)
                queue.Dequeue();
            queue.Enqueue(pt);

            Trajectory.Positions = new Vector3Collection(queue.Select(v => new Vector3((float)v.X, (float)v.Y, (float)v.Z)));
            OnPropertyChanged(nameof(Trajectory.Positions));

            var e1 = new LineBuilder();
            Vector3D w = size * new Vector3D(0.5f, 0.5f, 0.5f);
            w = CubeTransform.Transform(w);
            e1.Add(false, w.ToVector3(), (w + size* Configuration.Gravity).ToVector3());
            Gravity = e1.ToLineGeometry3D();
            OnPropertyChanged(nameof(Gravity));
        }


    }
}
