﻿using System.Windows;

namespace Spinner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        SimulationManager Manager => DataContext as SimulationManager;

        private void StartSimulationButton_Click(object sender, RoutedEventArgs e)
        {
            Manager.StartSimualtion();
        }
    }
}
