﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using SharpDX;
using Quaternion = SharpDX.Quaternion;
using Media3D = System.Windows.Media.Media3D;
using Common;

namespace Spinner
{
    public class SimulationManager
    {
        public Helix Helix { get; set; } = new Helix();

        public SimulationConfiguration Configuration => Helix.Configuration;

        private DispatcherTimer dispatcherTimer;

        private Quaternion currentQ;
        private Vector3 currentW;
        private Vector3D gravity;
        private Vector3 gravityDist; 
        private Matrix3x3 tensor;
        private Matrix3x3 tensorInv;

        const float precision = 0.01f;

        static readonly Quaternion Qstep = new Quaternion(new Vector3(0, 1, 0), 5);

        internal void StartSimualtion()
        {
            dispatcherTimer?.Stop();
            Helix.Reset();
            currentQ = new Media3D.Quaternion(new Vector3D(1, 0, 0), Configuration.CubeDiagonalAngle).ToSDXQuaternion() * Helix.DefaultQuaternion.ToSDXQuaternion();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)(precision * 1000));
            currentW = new Vector3(Configuration.CubeAngularVelocity, Configuration.CubeAngularVelocity, Configuration.CubeAngularVelocity);
            CalcualteTensor();

            var size = Configuration.CubeSize;
            gravity = size * size * size * Configuration.CubeDensity * Configuration.Gravity;
            gravityDist = size * new Vector3(0.5f, 0.5f, 0.5f);
            dispatcherTimer.Start();
        }


        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            var qi = currentQ;
            qi.Invert();
            var ri = new RotateTransform3D(new QuaternionRotation3D(qi.ToMedia3DQuaternion()));
            var gravityTrasformed = ri.Transform(gravity);
            var N = Configuration.IsGravityEnabled ? Vector3.Cross(gravityDist, gravityTrasformed.ToVector3()) : new Vector3(0,0,0);

            RungeKutty(ref currentW, (w) => Vector3.Transform(N + Vector3.Cross(Vector3.Transform(w, tensor), w), tensorInv));
            //RungeKutty(ref currentQ, (q) => 0.5f * Quaternion(currentW) * currentQ );
            RungeKutty(ref currentQ, (q) => 0.5f * currentQ * new Quaternion(currentW, 0));

            Helix.SetState(currentQ.ToMedia3DQuaternion());
        }

        //private Quaternion Quaternion(Vector3 w)
        //{
        //    var ri = new RotateTransform3D(new QuaternionRotation3D(currentQ.ToMedia3DQuaternion()));
        //    return new Quaternion(ri.Transform(w.ToVector3D()).ToVector3(), 0);
        //}

        private void RungeKutty(ref Quaternion q, Func<Quaternion, Quaternion> func)
        {
            Quaternion k1 = precision * func(q);
            Quaternion k2 = precision * func(q + 0.5f * k1);
            Quaternion k3 = precision * func(q + 0.5f * k2);
            Quaternion k4 = precision * func(q + k3);


            var delta = 1/6f * (k1 + 2f * k2 + 2f* k3 + k4);
            currentQ += delta;
            currentQ.Normalize();
        }

        private void RungeKutty(ref Vector3 w, Func<Vector3, Vector3> func)
        {
            Vector3 k1 = precision * func(w);
            Vector3 k2 = precision * func(w + k1 / 2);
            Vector3 k3 = precision * func(w + k2 / 2);
            Vector3 k4 = precision * func(w + k3);

            w += (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        }

        private void CalcualteTensor()
        {
            var s = Configuration.CubeSize;
            var s5 = s * s * s * s * s;
            var r = Configuration.CubeDensity;
            tensor = r * s5 * new Matrix3x3(
                2/3f, -1/4f, -1/4f,
                -1/4f, 2/3f, -1/4f,
                -1/4f, -1/4f, 2/3f
                );
            tensorInv = tensor;
            tensorInv.Invert();
        }
    }
}
