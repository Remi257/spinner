﻿using Common;
using System.Windows.Controls;
using System.Windows.Media.Media3D;

namespace Spinner
{
    public class SimulationConfiguration : ObservableObject
    {
        private bool isCubeVisible = true;
        private bool isDiagonalVisible =false;
        private bool isTrajectoryVisible = true;
        private bool isGravityVisible = false;
        private float cubeDensity = 1;
        private float cubeSize = 1;
        private float cubeDiagonalAngle = 0;
        private float cubeAngularVelocity = 1;
        private int trajectoryLength = 2000;
        private bool isGravityEnabled = true;
        public bool IsCubeVisible { get => isCubeVisible; set => SetValue(ref isCubeVisible, value); }
        public bool IsDiagonalVisible { get => isDiagonalVisible; set => SetValue(ref isDiagonalVisible, value); }
        public bool IsTrajectoryVisible { get => isTrajectoryVisible; set => SetValue(ref isTrajectoryVisible, value); } 
        public bool IsGravityVisible { get => isGravityVisible; set => SetValue(ref isGravityVisible, value); }
        public float CubeDensity { get => cubeDensity; set => SetValue(ref cubeDensity, value); }
        public float CubeSize { get => cubeSize; set => SetValue(ref cubeSize, value); }
        public float CubeDiagonalAngle { get => cubeDiagonalAngle; set => SetValue(ref cubeDiagonalAngle, value); }
        public float CubeAngularVelocity { get => cubeAngularVelocity; set => SetValue(ref cubeAngularVelocity, value); }
        public int TrajectoryLength { get => trajectoryLength; set => SetValue(ref trajectoryLength, value); }
        public bool IsGravityEnabled { get => isGravityEnabled; set => SetValue(ref isGravityEnabled, value); }
        public Vector3D Gravity => new Vector3D(0, 0, -1);
    }
}